﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace BoniniRafael_ex1GroceryList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //button to add item to have list 
        private void button1_Click(object sender, EventArgs e)
        {
            if(ItemNameTextBox.Text != string.Empty)
             {
                //add text from textbox to have list
                HaveListBox.Items.Add(ItemNameTextBox.Text);

            }

        }
        //button to add items to need list
        private void button2_Click(object sender, EventArgs e)
        {
            if(ItemNameTextBox.Text != string.Empty)
            {
                //add text from textbox to need list
                NeedListBox.Items.Add(ItemNameTextBox.Text);

            }
        }
        //button to move item from have list ot need list
        private void button3_Click(object sender, EventArgs e)
        {
            if(HaveListBox.SelectedIndex != -1)
            {
                //adding selected item to need list
                NeedListBox.Items.Add(HaveListBox.SelectedItem);
                //removing selected item from have list
                HaveListBox.Items.RemoveAt(HaveListBox.SelectedIndex);

            }

        }
        //button to move item from need list to have list
        private void button4_Click(object sender, EventArgs e)
        {
            if(NeedListBox.SelectedIndex != -1)
            {
                //adding selected item to have list
                HaveListBox.Items.Add(NeedListBox.SelectedItem);
                //removing selected item from need list
                NeedListBox.Items.RemoveAt(NeedListBox.SelectedIndex);
            }
        }
        // clear selection on need list if a item from have list is selected
        private void HaveListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if a item is selected
            if (HaveListBox.SelectedIndex != -1)
            {
                //clear selection from other list
                NeedListBox.ClearSelected();
            }
        }

        //clear selection on have list if a item from need list is selected
        private void NeedListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if a item is selected
            if(NeedListBox.SelectedIndex != -1)
            {
                //clear selection from other list
                HaveListBox.ClearSelected();
            }
        }

        //button to delete selected item
        private void button5_Click(object sender, EventArgs e)
        {
            //if there is a selected item on the have list run
            if(HaveListBox.SelectedIndex != -1)
            {
                //remove selected item
                HaveListBox.Items.RemoveAt(HaveListBox.SelectedIndex);
            }
            //if there is a selected item on the need list run
            else if(NeedListBox.SelectedIndex != -1)
            {
                //remove selected item
                NeedListBox.Items.RemoveAt(NeedListBox.SelectedIndex);
            }
        }

        //button to clear all items
        private void button6_Click(object sender, EventArgs e)
        {
            HaveListBox.Items.Clear();
            NeedListBox.Items.Clear();
            ItemNameTextBox.Text = string.Empty;
        }
        //exit application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //save to a local file
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog savefile1 = new SaveFileDialog();

            savefile1.RestoreDirectory = true;
            savefile1.FileName = "GroceryList";
            savefile1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            //deals with cancel error
            if (savefile1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter outstream = new StreamWriter(savefile1.FileName);

                //loop to output items from have list
                for (int i = 0; i < HaveListBox.Items.Count; i++)
                {
                    outstream.Write(HaveListBox.Items[i]);
                    outstream.WriteLine();

                }

                outstream.Write("asdasd.@,!@#$%^#@@#$asddasdneedlist");
                outstream.WriteLine();
                //loop to output items from need list
                for(int j = 0; j < NeedListBox.Items.Count; j++)
                {
                    outstream.Write(NeedListBox.Items[j]);
                    outstream.WriteLine();

                }

                outstream.Close();

            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
            OpenFileDialog filedialog1 = new OpenFileDialog();

            filedialog1.RestoreDirectory = true;
            filedialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            filedialog1.FileName = "GroceryList";

            //statement deals with exception of pressing cancel on dialog window
            if (filedialog1.ShowDialog() == DialogResult.OK)
            {
                NeedListBox.Items.Clear();
                HaveListBox.Items.Clear();

                //get all lines from the file to an array
                string file = filedialog1.FileName;
                string[] lines = File.ReadAllLines(file);

                //listbox variable to swap from one list to the other
                ListBox list = HaveListBox;

                //loop all lines from array
                for(int i = 0; i < lines.Count(); i++)
                {
                    //if there is the word "needlist" in a line swap to need list list box
                    if(lines[i] == "asdasd.@,!@#$%^#@@#$asddasdneedlist")
                    {
                        list = NeedListBox;
                    }

                    //only add to the list if the name of the item is not needlist
                    if (lines[i] != "asdasd.@,!@#$%^#@@#$asddasdneedlist")
                    {
                        //add to listbox
                        list.Items.Add(lines[i]);
                    }

                }
                //clear text box
                ItemNameTextBox.Text = string.Empty;
            }
        }
    }
}
