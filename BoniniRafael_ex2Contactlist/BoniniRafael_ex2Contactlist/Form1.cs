﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace BoniniRafael_ex2Contactlist
{
    public partial class Form1 : Form
    {
        public Users user
        {
            get
            {
                Users u = new Users();

                u.Firstname = Firstnamebox.Text;
                u.Lastname = LastNameBox.Text;
                u.Phone = Phonebox.Text;
                u.Email = Emailtextbox.Text;

                return u;
            }
            set
            {
                Firstnamebox.Text = value.Firstname;
                LastNameBox.Text = value.Lastname;
                Phonebox.Text = value.Phone;
                Emailtextbox.Text = value.Email;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        //button to exit application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //load from with default values on respective fields
        private void Form1_Load(object sender, EventArgs e)
        {
            user = new Users();
        }

        //adding contact to the list
        private void button1_Click(object sender, EventArgs e)
        {
            if (!AddOrNot())
            {

            }
            else
            {
                ListViewItem lvi = new ListViewItem();

                //item gets the information
                lvi.Tag = user;
                //text displayed on the item from the toString
                lvi.Text = lvi.Tag.ToString();

                lvi.ImageIndex = 0;

                listView1.Items.Add(lvi);
            }

            //hide modify button
            button3.Visible = false;
        }

        //click to select large icon
        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.View = View.LargeIcon;
            //check largetoostrip and uncheck small
            largeToolStripMenuItem.Checked = true;
            smallToolStripMenuItem.Checked = false;
        }

        //click to select small icon
        private void smallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.View = View.SmallIcon;
            //check smalltoolstrip and uncheck large
            largeToolStripMenuItem.Checked = false;
            smallToolStripMenuItem.Checked = true;
        }

        //double click an item will display its information to me modified
        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            button3.Visible = true;

            user = (Users)listView1.SelectedItems[0].Tag;
        }

        //remove selected item
        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count != 0)
            {
                listView1.SelectedItems[0].Remove();
            }
        }

        //when select a different contact
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //hide modify button
            button3.Visible = false;

        }
        //click modify button
        private void button3_Click(object sender, EventArgs e)
        {

            if (!AddOrNot()) { }
            else
            {
                //update selected contact information
                listView1.SelectedItems[0].Tag = user;
                //update text displayed for the selected contact
                listView1.SelectedItems[0].Text = user.ToString();

                //hide modify button
                button3.Visible = false;
            }
        }

        //empty list view and reset user input
        private void clearAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.Clear();
            user = new Users();
            button3.Visible = false;
        }

        //method to validate user input
        public bool AddOrNot()
        {
            //variables for tryparse
            int result;
            bool isnumber;

            //regular expressions
            Regex nameRegex = new Regex("^[a-zA-Z]*$");
            Regex phoneRegex = new Regex("^[0 - 9] *$");
            Regex emailRegex = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                        +"@"
                                        +@"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");


            if (!nameRegex.IsMatch(user.Firstname))
            {
                MessageBox.Show("Incorrect characters in firstname");
                return false;

            }
            else if (!nameRegex.IsMatch(user.Lastname))
            {
                MessageBox.Show("Incorrect characters in lastname");
                return false;
            }
            else if (!(isnumber = Int32.TryParse(user.Phone, out result)))
            {
                MessageBox.Show("Numbers only!");
                return false;
            }
            else if (user.Phone.Length != 10)
            {
                MessageBox.Show("Wrong phone number size");
                return false;
            }
            else if (!emailRegex.IsMatch(user.Email))
            {
                MessageBox.Show("Invalid email");

                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
