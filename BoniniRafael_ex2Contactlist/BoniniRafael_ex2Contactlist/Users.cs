﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoniniRafael_ex2Contactlist
{
    public class Users
    {
        string firstname = "firstname";
        string lastname = "lastname";
        string phone = "1231231234";
        string email = "exampleemail@fullsail.edu";

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }

            set
            {
                phone = value;
            }
        }

        public string Lastname
        {
            get
            {
                return lastname;
            }

            set
            {
                lastname = value;
            }
        }

        public string Firstname
        {
            get
            {
                return firstname;
            }

            set
            {
                firstname = value;
            }
        }


        public override string ToString()
        {
            return "Name:" + lastname + ", " + firstname;
        }
    }
}
