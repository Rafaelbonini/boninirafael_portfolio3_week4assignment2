﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoniniRafael_ex3TicTacToe
{
    public partial class Form1 : Form
    {
        int turn = 0;

        Userdata user
        {

            get
            {
                Userdata u = new Userdata();

                u.Button1 = button1.Text;
                u.Button2 = button2.Text;
                u.Button3 = button3.Text;
                u.Button4 = button4.Text;
                u.Button5 = button5.Text;
                u.Button6 = button6.Text;
                u.Button7 = button7.Text;
                u.Button8 = button8.Text;
                u.Button9 = button9.Text;
                u.Button10 = button10.Text;
                u.Button11 = button11.Text;
                u.Button12 = button12.Text;
                u.Button13 = button13.Text;
                u.Button14 = button14.Text;
                u.Button15 = button15.Text;
                u.Button16 = button16.Text;


                return u;

            }
            set
            {
                button1.Text = value.Button1 ;
                button2.Text = value.Button2;
                button3.Text = value.Button3;
                button4.Text = value.Button4;
                button5.Text = value.Button5;
                button6.Text = value.Button6;
                button7.Text = value.Button7;
                button8.Text = value.Button8;
                button9.Text = value.Button9;
                button10.Text = value.Button10;
                button11.Text = value.Button11;
                button12.Text = value.Button12;
                button13.Text = value.Button13;
                button14.Text = value.Button14;
                button15.Text = value.Button15;
                button16.Text = value.Button16;

            }


        }

        public Form1()
        {
            InitializeComponent();

            user = new Userdata();

            //instantiate second form
            StartPlayer begin = new StartPlayer();

            begin.startOption += HandlerstartOption;
            begin.startOption2 += HandlerstartOption2;

            begin.ShowDialog();

            //print first to start
            if(turn == 1)
            {
                label2.Text = "O";
            }
            else
            {
                label2.Text = "X";
            }
        }

        
        //handler
        public void HandlerstartOption(object sender, EventArgs e)
        {
            turn = 0;

        }
        //handler
        public void HandlerstartOption2(object sender, EventArgs e)
        {
            turn = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //
            Printturn(button1);
            Wincondition();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Printturn(button2);
            Wincondition();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Printturn(button3);
            Wincondition();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Printturn(button4);
            Wincondition();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Printturn(button5);
            Wincondition();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Printturn(button6);
            Wincondition();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Printturn(button7);
            Wincondition();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Printturn(button8);
            Wincondition();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Printturn(button9);
            Wincondition();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Printturn(button10);
            Wincondition();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Printturn(button11);
            Wincondition();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Printturn(button12);
            Wincondition();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Printturn(button13);
            Wincondition();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Printturn(button14);
            Wincondition();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Printturn(button15);
            Wincondition();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Printturn(button16);
            Wincondition();
        }


        public void Printturn(Button currentButton)
        {
            if (turn == 1)
            {
                currentButton.Text = "O";
                //disable pressed button
                currentButton.Enabled = false;
                label2.Text = "X";
                //decrement turn counter to swap turns
                turn--;
            }
            else
            {
                currentButton.Text = "X";
                currentButton.Enabled = false;
                label2.Text = "O";
                //increment turn counter to swap turns
                turn++;
            }

        }

        public void Wincondition()
        {
            if(button1.Text == "X" && button2.Text == "X" && button3.Text == "X" && button4.Text == "X")
            {
                MessageBox.Show("X WON THE GAME!!");
                Close();
            }
            else if(button1.Text == "O" && button2.Text == "O" && button3.Text == "O" && button4.Text == "O")
            {
                MessageBox.Show("O WON THE GAME!!");
                Close();
            }
            else if (button5.Text == "X" && button6.Text == "X" && button7.Text == "X" && button8.Text == "X")
            {
                MessageBox.Show("X WON THE GAME!!");
                Close();
            }
            else if (button5.Text == "O" && button6.Text == "O" && button7.Text == "O" && button8.Text == "O")
            {
                MessageBox.Show("O WON THE GAME!!");
                Close();
            }
            else if (button9.Text == "X" && button10.Text == "X" && button11.Text == "X" && button12.Text == "X")
            {
                MessageBox.Show("X WON THE GAME!!");
                Close();
            }
            else if (button9.Text == "O" && button10.Text == "O" && button11.Text == "O" && button12.Text == "O")
            {
                MessageBox.Show("O WON THE GAME!!");
                Close();
            }
            else if (button13.Text == "X" && button14.Text == "X" && button15.Text == "X" && button16.Text == "X")
            {
                MessageBox.Show("X WON THE GAME!!");
                Close();
            }
            else if (button13.Text == "O" && button14.Text == "O" && button15.Text == "O" && button16.Text == "O")
            {
                MessageBox.Show("O WON THE GAME!!");
                Close();
            }
            else if (button1.Text == "X" && button6.Text == "X" && button11.Text == "X" && button16.Text == "X")
            {
                MessageBox.Show("X WON THE GAME!!");
                Close();
            }
            else if (button1.Text == "O" && button6.Text == "O" && button11.Text == "O" && button16.Text == "O")
            {
                MessageBox.Show("O WON THE GAME!!");
                Close();
            }
            else if (button4.Text == "X" && button7.Text == "X" && button10.Text == "X" && button13.Text == "X")
            {
                MessageBox.Show("X WON THE GAME!!");
                Close();
            }
            else if (button4.Text == "O" && button7.Text == "O" && button10.Text == "O" && button13.Text == "O")
            {
                MessageBox.Show("O WON THE GAME!!");
                Close();
            }
            //every square is not empty and no one won = draw
            else if(button1.Text != string.Empty && button2.Text != string.Empty && button3.Text != string.Empty && button4.Text != string.Empty
                && button5.Text != string.Empty && button6.Text != string.Empty && button7.Text != string.Empty && button8.Text != string.Empty
                && button9.Text != string.Empty && button10.Text != string.Empty && button11.Text != string.Empty && button12.Text != string.Empty
                && button13.Text != string.Empty && button14.Text != string.Empty && button15.Text != string.Empty && button16.Text != string.Empty)
            {
                MessageBox.Show("DRAAAW! NO ONE WON!");
                Close();
            }


        }
    }
}
