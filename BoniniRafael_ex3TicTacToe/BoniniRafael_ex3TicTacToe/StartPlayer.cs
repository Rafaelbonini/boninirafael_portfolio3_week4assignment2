﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoniniRafael_ex3TicTacToe
{
    public partial class StartPlayer : Form
    {
        public EventHandler startOption;
        public EventHandler startOption2;

        public StartPlayer()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if(startOption != null)
            {
                startOption(this, new EventArgs());
            }

            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(startOption2 != null)
            {
                startOption2(this, new EventArgs());
            }

            Close();
        }
    }
}
